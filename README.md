# next-docker-multi-stage-gitlab-ci-cd-example

This is an example [Next.js](https://nextjs.org/) project with a GitLab CI/CD pipeline that does the following:

- installs npm packages and builds static assets
- runs ESLint, TypeScript, and Cypress
- builds a Docker image for deployment
- pushes the Docker image to the [GitLab Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/)

In an effort to improve speed, it uses a GitLab [directed acyclic graph pipeline](https://docs.gitlab.com/ee/ci/directed_acyclic_graph/index.html) and [Docker multi-stage builds](https://docs.docker.com/develop/develop-images/multistage-build/). It also uses [Docker BuildKit](https://docs.docker.com/develop/develop-images/build_enhancements/) to make caching easier. (With BuildKit, cached layers will be automatically pulled when needed. Without BuildKit, images used for caching need to be explicitly pulled.)

## Usage

- push commit
- see pipeline run here: https://gitlab.com/saltycrane/next-docker-multi-stage-gitlab-ci-cd-example/-/pipelines

## References

- https://docs.docker.com/develop/develop-images/multistage-build/
- https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#making-docker-in-docker-builds-faster-with-docker-layer-caching
- https://docs.gitlab.com/ee/ci/directed_acyclic_graph/index.html
- https://docs.gitlab.com/ee/ci/yaml/README.html#needs
- https://testdriven.io/blog/faster-ci-builds-with-docker-cache/
- https://docs.docker.com/engine/reference/commandline/build/#specifying-external-cache-sources
- https://docs.docker.com/develop/develop-images/build_enhancements/
